---
title: Services
template: main-full.html
---

## Introduction

You're creating a music player and would like the music to continue even if the user is looking at a different application.

Or your application manages data or an external sensor that other applications need to access.

Android Services can help!

In this module, we'll create Android services that bridge the gap between applications and can keep running even if the current application is not visible.

The sample applications included in this directory are:

* Service - general service examples - all in same application
* AndroidService - separate application only hosting a remote service
* AndroidClient - separate application that talks with the remote service

!!! note

    Music in example downloaded from
    https://www.free-stock-music.com/alexander-nakarada-one-bard-band.html

    LICENSE

    One Bard Band by Alexander Nakarada | https://www.serpentsoundstudios.com
    
    Music promoted by https://www.free-stock-music.com
    
    Attribution 4.0 International (CC BY 4.0): https://creativecommons.org/licenses/by/4.0/

## Videos

Total video time for this module: 1:08:48

            
### Services: Lecture (Fall 2021) (28:14)

<iframe width="800" height="450" src="https://www.youtube.com/embed/0mn-AJ-fcmw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                

### Services: Example (Fall 2021) (35:42)

<iframe width="800" height="450" src="https://www.youtube.com/embed/4br0VOPIzu8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                

### Services: Remote Example (Fall 2021) (04:52)

<iframe width="800" height="450" src="https://www.youtube.com/embed/w_Bxk6cBAdE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            
## Example Source

See: [https://gitlab.com/android-development-2022-refresh/services](https://gitlab.com/android-development-2022-refresh/services)
